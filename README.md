# Concurrent access and locks in Python/Redis/Elasticsearch

## Goal

There is a system submitting jobs to AWS Textract.

Textract has some limits, specifically: rate limit (e.g. 10 req/sec) and hard limit (e.g. max of 600 jobs running for an account).

There is a lambda listening a queue and submitting the received jobs. 

Since this lambda is triggered by the queue, in a given moment can be more than one instance running.

The goal is to avoid errors due to these limits.

## The solution

This solution implements Redis to store a lock and register jobs in Elasticsearch to know the number of running jobs and avoid the limit.

Also has  error control when rate limit is reached, retrying after a delay.

The composition includes a Redis and an ES instances, a process simulating the removal of finished jobs (the no add process), and a few processes simulating lambdas submitting jobs.

## Run it

Just clone the repo and run:

``` shell
docker-compose up
```

If the container image is not available in Docker Hub, it can be built:

``` shell
cd client-py
docker build -t yourusername/redis-py .
```

If the image is built, modify the image name in the docker-compose.yaml file.

The output will show all the activity, indicating the process number in the logs for easy following.

## Details

The waiting times are random, so we ensure the processes do not set or read data at the same time.

If the available jobs are greater than zero but less than the number of jobs a process has to submit, then a partial submission will occur.
