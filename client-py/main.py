#!/usr/bin/env python3

from pottery import  Redlock, RedisDict
from redis import Redis
import time
import random as rn
import sys
import os

from elasticsearch import Elasticsearch
#from elasticsearch import ElasticsearchException
from datetime import datetime as dt

key = 'gilda'
max_jobs = 10
redis_host = 'redis'
elasticsearch_host = 'elasticsearch'

if 'REDIS_HOST' in os.environ:
    redis_host = os.environ['REDIS_HOST']
if 'ES_HOST' in os.environ:
    elasticsearch_host = os.environ['ES_HOST']

def print_title(title):
    print(f"\n\n{title}\n")
    print('='*80)
    print("\n")

def print_message(process, message):
    print(f"    [{process}] {message}")

def run_test(process, add=True):
    print_title(f"Process {process} - add process={add}")
    # add process will add random jobs when possible in a loop to simulate queue inputs
    # not add process will remove jobs in a rate of 10%
    r=[None]*50
    rl=[None]*50

    document_index_suffix = 0

    jobs_queue = []

    if add:
        init_delay = rn.randint(1,5)
        max_cycles = rn.randint(2,5)
        print_message(process, f"waiting {init_delay} secs to start {max_cycles} cycle/s")
        time.sleep(init_delay)
    else:
        max_cycles = 50

    print_message(process, 'starting')
    print_message(process, 'creating es...')
    print_message(process, f"http://{elasticsearch_host}:9201")
    es = Elasticsearch(f"http://{elasticsearch_host}:9201")
    print_message(process, 'checking es...')
    es_try = 0
    while not es.ping() and es_try < 10:
        print_message(process, '    es not ready...')
        time.sleep(5)
        es_try += 1
    if es_try>=10:
        raise Exception('ES not ready!')
    print_message(process, 'creating redis...')
    r[process] = Redis(host= redis_host, port= 6379)
    print_message(process, f"{redis_host} 6379")
    print_message(process, 'creating redlock with 20sec ttl...')
    rl[process] = Redlock(masters={r[process]},key=key, auto_release_time=20000, context_manager_timeout=1000)
    print_message(process, f"key {rl[process].key} is locked? {rl[process].locked()}")

    cycle = 0
    while cycle < max_cycles:
        print_message(process, f"cycle {cycle}/{max_cycles}...")
        cycle += 1

        if add:
            # here we should be reading the queue looking for jobs
            job2sched = rn.randint(1,10)
            print_message(process, f"using key {key} -- jobs to schedule {job2sched}")
            for i in range(job2sched):
                idx = f"{process}_{document_index_suffix}_{dt.timestamp(dt.now())}"
                print_message(process, f"     creating document {idx} in es")
                try:
                    resp = es.index(index="textract_submit",id=idx,document={"doc": {"status": "NEW", "textract_id": 6565665}},request_timeout=30,)
                    jobs_queue.append(idx)
                except Exception as e:
                    raise e
                document_index_suffix += 1
        else:
            job2sched = 1

        print_message(process, 'getting lock...')
        try:
            remaining_jobs = job2sched
            while remaining_jobs > 0:
            # the number of jobs will be added to redis and then (if success) we will be ready and ok_to_go
                ok_to_go = False
                max_tries = 5
                try_number = 0
                try_waiting_time = 10
                while not ok_to_go and try_number < max_tries:
                    try_number += 1
                    print_message(process, f"try {try_number}/{max_tries}...")
                    if try_number > 0:
                        time.sleep(try_waiting_time)
                        with rl[process]:
                            print_message(process, f"lock acquired, key is locked? {rl[process].locked()}")

                            value = 0

                            if add:
                                print_message(process, 'Getting proccessing jobs...')
                                resp = es.search(index="textract_submit",query={"match": {"status": "PROCESSING"}}, filter_path=['hits.total.value'])
                                if len(resp) > 0:
                                    if not ('hits' in resp and 'total' in resp['hits']):
                                        raise Exception(f"Wrong answer format: {resp}")
                                    value = resp['hits']['total']['value']
                                print_message(process, f"checking whether {max_jobs} - {value} is greater than {job2sched}")
                                if (max_jobs - int(value)) >= job2sched:
                                    # all jobs can bu submitted
                                    print_message(process, f"prev value {value} -- new value {int(value)+job2sched} ---------------------------")
                                    r[process].set(key,int(value)+job2sched)
                                    final_delay = rn.randint(1,2)
                                    print_message(process, f"waiting {final_delay} secs after set keeping the lock")
                                    time.sleep(final_delay)
                                    ok_to_go = True
                                    remaining_jobs = 0
                                    print_message(process, f"submitting {job2sched} jobs to Textract...")
                                    try:
                                        for j in range(job2sched):
                                            print_message(process, '.')
                                            i = jobs_queue.pop()
                                            resp = es.update(index="textract_submit",id=i,doc={"status": "PROCESSING"},request_timeout=30,)
                                            time.sleep(.5)
                                    except Exception as e:
                                        raise e
                                    job2sched = remaining_jobs
                                elif (max_jobs - int(value)) > 0:
                                    # at least one job can bu submitted
                                    remaining_jobs = job2sched - (max_jobs - int(value))
                                    job2sched = (max_jobs - int(value))
                                    print_message(process, f"prev value {value} -- ne value {int(value)+job2sched} PARTIAL --------------------")
                                    r[process].set(key,int(value)+job2sched)
                                    final_delay = rn.randint(1,2)
                                    print_message(process, f"waiting {final_delay} secs after set keeping the lock")
                                    time.sleep(final_delay)
                                    ok_to_go = True
                                    print_message(process, f"submitting {job2sched} jobs to Textract...")
                                    try:
                                        for j in range(job2sched):
                                            print_message(process, '.')
                                            i = jobs_queue.pop()
                                            resp = es.update(index="textract_submit",id=i,doc={"status": "PROCESSING"},request_timeout=30,)
                                            time.sleep(.5)
                                    except Exception as e:
                                        raise e
                                    job2sched = remaining_jobs
                                else:
                                   print_message(process, f"condition not met, waiting antoher cycle")
                            else:
                                finished_perc = round(rn.randrange(0,50)/100,2)
                                try:
                                    # n jobs are get randomly to be terminated, in a real situation the terminated jobs
                                    # have to be the ones actually being terminated!
                                    resp = es.search(index="textract_submit",query={"match": {"status": "PROCESSING"}}, size=600, filter_path=['hits.hits._id'])
                                    if len(resp) > 0:
                                        if not ('hits' in resp and 'hits' in resp['hits']):
                                            raise Exception(f"Wrong answer format: {resp}")
                                        finished_jobs = round((len(resp['hits']['hits']) * finished_perc) + 0.5)
                                        print_message(process, f"finishing {finished_perc}% ~ {finished_jobs} job/s (TOTAL: {len(resp['hits']['hits'])})")
                                        for j in resp['hits']['hits'][0:finished_jobs]:
                                            i = j['_id']
                                            respf = es.update(index="textract_submit",id=i,doc={"status": "FINISHED"},request_timeout=30,)
                                        print_message(process, f"working jobs {len(resp['hits']['hits']) - finished_jobs} ---------------------------")
                                except Exception as e:
                                    raise e
                                ok_to_go = True
                if not ok_to_go:
                    raise Exception('max tries reached!')
        except Exception as e:
            raise e
        print_message(process, f"released, key is locked? {rl[process].locked()}")
        if add:
            final_delay = rn.randint(5,10)
        else:
            final_delay = rn.randint(10,20)
        print_message(process, f"waiting {final_delay} secs with no lock")
        time.sleep(final_delay)

if __name__ == "__main__":
    print_message(None, 'Waiting 10 secs for redis and es to start up...')
    time.sleep(10)
    if len(sys.argv) > 1:
        process = sys.argv[1]
    else:
        process = rn.randint(0,100)
    if process != "-1":
        run_test(int(process))
    else:

        run_test(int(process), add=False)

        print_title('Final result')
        r = Redis(host= redis_host, port= 6379)
        rl = Redlock(masters={r},key=key, auto_release_time=20000, context_manager_timeout=1000)
        print_message(process, 'getting lock...')
        with rl:
            resp = es.search(index="textract_submit",query={"match": {"status": "PROCESSING"}}, filter_path=['hits.total.value'])
            if len(resp) > 0:
                if not ('hits' in resp and 'total' in resp['hits']):
                    raise Exception(f"Wrong answer format: {resp}")
                value = resp['hits']['total']['value']
            print_message(process, f"Final value is {value}")

    print_message(process, "THE END")
